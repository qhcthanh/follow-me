//
//  Extension.swift
//  FollowMe
//
//  Created by Nguyen Thinh on 4/27/16.
//  Copyright © 2016 The Simple Studio. All rights reserved.
//

import Foundation

extension String {
    func isNumberPhone() -> Bool {
        if let _ = Int(self) {
            return true
        }
        return false
    }
    
    func trim() -> String {
        return self.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
    }
    
    func count() -> Int {
        return self.characters.count
    }

}

