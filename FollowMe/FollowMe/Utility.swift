//
//  Utility.swift
//  FollowMe
//
//  Created by Developer on 4/18/16.
//  Copyright © 2016 The Simple Studio. All rights reserved.
//


import Foundation
import UIKit
import JLToast
import GoogleMaps

class Utility {
    
    class func borderRadiusView(radius : CGFloat, view : UIView) {
        view.layer.cornerRadius = radius
        view.layer.masksToBounds = true
    }
    
    class func showToastWithMessage(mesage: String, duration: NSTimeInterval = JLToastDelay.ShortDelay) {
        JLToast.makeText(mesage, duration: duration).show()
    }
    
    class func showAlertWithMessageOKCancel(message: String,title: String,sender: UIViewController, doneAction: ( () -> Void )?, cancelAction: ( () -> Void )? ) {
        dispatch_async(dispatch_get_main_queue(), {
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
            let okAction: UIAlertAction = UIAlertAction(title: "OK", style: .Default) { action -> Void in
                if let action = doneAction {
                    action()
                }
            }
            alert.addAction(okAction)
            let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) {
                action -> Void in
                if let action = cancelAction {
                    action()
                }
            }
            alert.addAction(cancelAction)
            sender.presentViewController(alert, animated: true, completion: { () in  })
        })
    }
    
    
    class func navigateToVC(from : UIViewController, vc : UIViewController, direction : String = kCATransitionFromRight) {
        
        vc.view.frame = from.view.frame
        from.addChildViewController(vc)
        from.view.addSubview(vc.view)
        // Move the view move in from the right
        CATransaction.begin()
        CATransaction.setAnimationDuration(0.3)
        let transition = CATransition()
        transition.type = kCATransitionPush
        transition.subtype = direction
        vc.view.layer .addAnimation(transition, forKey: kCATransition)
        vc.didMoveToParentViewController(from)
        CATransaction.commit()
    }
    
    class func openAuthentication(vc: UIViewController) {
        if let auth = vc.storyboard?.instantiateViewControllerWithIdentifier("AuthenticationController") {
                vc.presentViewController(auth, animated: true, completion: nil)
        } else {
            Utility.showToastWithMessage("Fail open this view")
        }
    }
    
    class func openAuthenticated(vc: UIViewController) {
        if let auth = vc.storyboard?.instantiateViewControllerWithIdentifier("AuthenticatedController") {
            vc.presentViewController(auth, animated: true, completion: nil)
        } else {
            Utility.showToastWithMessage("Fail open this view")
        }
    }
    
//    class func openAuthenticatedFlow() {
//        let authenticationSB = UIStoryboard(name: "Authenticated", bundle: NSBundle.mainBundle())
//        let initVC = authenticationSB.instantiateInitialViewController()
//        appDelegate.window?.rootViewController = initVC
//    }
//    
//    class func openAuthenticationFlow() {
//        let authenticationSB = UIStoryboard(name: "Authentication", bundle: NSBundle.mainBundle())
//        let initVC = authenticationSB.instantiateInitialViewController()
//        appDelegate.window?.rootViewController = initVC
//    }
    
    //    class func getSideMenuNavigationC() -> UISideMenuNavigationController {
    //        let authenticationSB = UIStoryboard(name: "Authenticated", bundle: NSBundle.mainBundle())
    //        let sideMenuNavigationC = authenticationSB.instantiateViewControllerWithIdentifier("SideMenuNavigationControllerID") as! UISideMenuNavigationController
    //        return sideMenuNavigationC
    //    }
    
    class func blueColor() -> UIColor {
        return UIColor(red: 14.0/255.0, green: 159.0/255.0, blue: 193.0/255.0, alpha: 1.0)
    }
    
    class func lightBlueColor() -> UIColor {
        return UIColor(red: 169.0/255.0, green: 219.0/255.0, blue: 239.0/255.0, alpha: 1.0)
    }
    
    class func purpleColor() -> UIColor {
        return UIColor(red: 158.0/255.0, green: 103.0/255.0, blue: 201.0/255.0, alpha: 1.0)
    }
    
    class func grayColor() -> UIColor {
        return UIColor(red: 102.0/255.0, green: 102.0/255.0, blue: 102.0/255.0, alpha: 1.0)
    }
    
    class func currencyList() -> [String] {
        var myArray = [String]()
        if let path = NSBundle.mainBundle().pathForResource("Currency", ofType: "plist") {
            let dict = NSDictionary(contentsOfFile: path)
            for (_, value) in dict! {
                myArray.append(value as! String)
            }
        }
        return myArray
    }
    
    class func locationList() -> [String] {
        var myArray = [String]()
        if let path = NSBundle.mainBundle().pathForResource("Location", ofType: "plist") {
            let dict = NSDictionary(contentsOfFile: path)
            for (_, value) in dict! {
                myArray.append(value as! String)
            }
        }
        return myArray
    }
    
//    class func startIndicator(view: UIViewController) {
//        indicator.frame = CGRect(x: (view.view.frame.maxX)/2 - 100, y: (view.view.frame.maxY)/2 - 100, width: 200, height: 200)
//        indicator.type = NVActivityIndicatorType.BallClipRotate
//        view.view.addSubview(indicator)
//        indicator.startAnimation()
//    }
//    
//    class func stopIndicator(view: UIViewController) {
//        indicator.removeFromSuperview()
//        indicator.stopAnimation()
//    }
//    
    class func checkInTouch(crd: CGPoint, frame: CGRect) -> Bool {
        if crd.x > frame.minX && crd.x < frame.maxX && crd.y > frame.minY && crd.y < frame.maxY {
            return true
        }
        return false
    }
    
    class func scaleImage(image: UIImage, toSize newSize: CGSize) -> (UIImage) {
        let newRect = CGRectIntegral(CGRectMake(0,0, newSize.width, newSize.height))
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
        let context = UIGraphicsGetCurrentContext()
        CGContextSetInterpolationQuality(context, .High)
        let flipVertical = CGAffineTransformMake(1, 0, 0, -1, 0, newSize.height)
        CGContextConcatCTM(context, flipVertical)
        CGContextDrawImage(context, newRect, image.CGImage)
        let newImage = UIImage(CGImage: CGBitmapContextCreateImage(context)!)
        UIGraphicsEndImageContext()
        return newImage
    }
    
    class func nsData2UIImage(data: NSData) -> UIImage? {
        return UIImage(data: data)
    }
    
    class func showIndicatorForView(view: UIView) {
        let indicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.WhiteLarge)
        indicatorView.tag = 2601
        indicatorView.center = view.center
        indicatorView.startAnimating()
        view.userInteractionEnabled = false
        view.addSubview(indicatorView)
    }
    
    class func removeIndicatorForView(view: UIView) {
        dispatch_async(dispatch_get_main_queue(),{
            let view1 = view.viewWithTag(2601) as? UIActivityIndicatorView
            if let indicator = view1 {
                indicator.stopAnimating()
                indicator.removeFromSuperview()
            }
            view.userInteractionEnabled = true
        })
    }
    
    class func getArrayContact(contacts: [ContactInfo]) -> NSArray? {
        if let user = currentUser {
            var dict: [NSDictionary] = [NSDictionary]()
            for contact in contacts {
                if var img = contact.image {
                    img = Utility.scaleImage(img,toSize: CGSize(width: 50,height: 50))
                    let imageData = UIImageJPEGRepresentation(img, 1.0)
                    //user.photo = imageData
                    let base64String = imageData!.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
                    let ct = [
                        "name": contact.name,
                        "phone": contact.phone,
                        "image": base64String
                    ]
                    dict.append(ct)
                } else {
                    let ct = [
                        "name": contact.name,
                        "phone": contact.phone,
                        ]
                    dict.append(ct)
                }
            }
            return dict
        } else {
            return nil
        }
    }
    
    //MARK: func
    class func backgroundThread(delay: Double = 0.0, background: (() -> Void)? = nil, completion: (() -> Void)? = nil) {
        dispatch_async(dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.rawValue), 0)) {
            if(background != nil){ background!(); }
            
            let popTime = dispatch_time(DISPATCH_TIME_NOW, Int64(delay * Double(NSEC_PER_SEC)))
            dispatch_after(popTime, dispatch_get_main_queue()) {
                if(completion != nil){ completion!(); }
            }
        }
    }
    
    class func saveContext(success: (() -> Void)? , fail: (() -> Void)? ) {
        if let _ = try? appDelegate.managedObjectContext.save() {
            if let success = success {
                success()
            }
        } else {
            if let fail = fail {
                fail()
            }
        }
    }
    
    //MARK: Tim duong di
    class func findDirection(toPlaceID: String, fromPlaceID: String) {
        // return [poylyline]
    }
    
    //MARK:
    class func addPolylineToMap(inout mapView: GMSMapView!, toPlaceID: String, fromPlaceID: String) {
        // [Polyline] 
        // Add To Map
    }

}