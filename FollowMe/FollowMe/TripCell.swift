//
//  TripCell.swift
//  FollowMe
//
//  Created by Developer on 5/5/16.
//  Copyright © 2016 The Simple Studio. All rights reserved.
//

import UIKit

@objc protocol TripCellProtocol {
    func acceptAction(trip: TripInfo?)
    func denyAction(trip: TripInfo?)
}

@objc protocol HistoryCellProtocol {
    func infoTripAction(trip: TripInfo?)
    func deleteTripAction(trip: TripInfo?)
}

class TripCell: UITableViewCell {
    @IBOutlet weak var nameTripLabel: UILabel!
    @IBOutlet weak var friendLabel: UILabel!
    @IBOutlet weak var toPlaceLabel: UILabel!
    @IBOutlet weak var fromPlaceLabel: UILabel!
    @IBOutlet weak var accepteButton: UIButton!
    @IBOutlet weak var denyButton: UIButton!
    
    var trip: TripInfo?
    var delegate: TripCellProtocol?
    var delegateHistory: HistoryCellProtocol?
    
    func cleanUI() {
        nameTripLabel.text = ""
        friendLabel.text = ""
        fromPlaceLabel.text = ""
        toPlaceLabel.text = ""
        
    }
    
    func renderUI() {
        if let trip = trip {
            nameTripLabel.text = trip.name
            if let name = trip.userName {
                friendLabel.text = trip.userName
            } else {
                friendLabel.text = "Đang cập nhật"
            }
            fromPlaceLabel.text = "Từ " + (trip.fromPlace?.name)!
            toPlaceLabel.text = "Đến " + (trip.toPlace?.name)!
        }
    }
    
    @IBAction func acceptAction(sender: AnyObject) {
        delegate?.acceptAction(trip)
    }
    
    @IBAction func denyAction(sender: AnyObject) {
        delegate?.denyAction(trip)
    }
    
    @IBAction func infoTripAction(sender: AnyObject) {
        delegateHistory?.infoTripAction(trip)
    }
    
    @IBAction func deleteTripAction(sender: AnyObject) {
        delegateHistory?.deleteTripAction(trip)
    }
}
