//
//  ViewController.swift
//  FollowMe
//
//  Created by Developer on 4/12/16.
//  Copyright © 2016 The Simple Studio. All rights reserved.
//

import Firebase
import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class SignInViewController: UIViewController, GIDSignInDelegate, GIDSignInUIDelegate {
    
    //MARK: UIElement
    @IBOutlet weak var textPassLogin: UITextField!
    @IBOutlet weak var textEmailLogin: UITextField!
    
    // MARK: Class's properties
    var facebookReadPermissions = ["public_profile","email","user_friends"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(SignInViewController.endEditing))
        tapGesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapGesture)
        // Setup delegates
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
    }

    func endEditing() {
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MASK: UIAction
    @IBAction func touchSignin(sender: AnyObject) {
        if textEmailLogin.text == "" || textPassLogin == "" {
            Utility.showToastWithMessage("Email and Password could not be empty")
        }
        else {
            Utility.showIndicatorForView(self.view)
            rootRef.authUser(textEmailLogin.text, password: textPassLogin.text,
                             withCompletionBlock: { error, authData in
                if error != nil {
                    if let errorCode = FAuthenticationError(rawValue: error.code) {
                        switch (errorCode) {
                        case .UserDoesNotExist:
                            Utility.showToastWithMessage(kErrorSignInUserDoesNotExist)
                        case .InvalidEmail:
                            Utility.showToastWithMessage(kErrorSignInInvalidEmail)
                        case .InvalidPassword:
                            Utility.showToastWithMessage(kErrorSignInInvalidPassword)
                        case .NetworkError:
                            Utility.showToastWithMessage(kErrorNetwork)
                        default:
                            Utility.showToastWithMessage(kErrorAuthenticationDefault)
                        }
                    }
                    // There was an error logging in to this account
                    Utility.removeIndicatorForView(self.view)
                    Utility.showToastWithMessage("Email or Password is not corrected")
                } else {
                    // Load data here
                    userRef.childByAppendingPath(authData.uid).observeSingleEventOfType(.Value, withBlock: {
                        snapshot in
                        // If user has exists in firebase
                        if let data = snapshot.value as? NSDictionary {
                            // if user has exists in coredata
                            if let user = User.getUserWithID(appDelegate.managedObjectContext, uID: authData.uid) {
                                user.reloadData(snapshot.key, data: data)
                                currentUser = user
                                // open Authenticated
                                Utility.removeIndicatorForView(self.view)
                                Utility.openAuthenticated(self)
                            } // user hasn't in coredata
                            else {
                                currentUser = User.createInManagedObjectContext(appDelegate.managedObjectContext, uid: snapshot.key, data: data)
                                // open Authenticated
                                Utility.removeIndicatorForView(self.view)
                                Utility.openAuthenticated(self)
                            }
                        }
                        
                    })
                }
                
            })
        }
    }
    
    @IBAction func facebookLoginAction(sender: AnyObject) {
        let facebookLogin : FBSDKLoginManager = FBSDKLoginManager()
        facebookLogin.logInWithReadPermissions(facebookReadPermissions, fromViewController: self, handler: {
            (facebookResult, facebookError) -> Void in
            if facebookError != nil {
                print("Facebook login failed. Error \(facebookError)")
                Utility.showToastWithMessage(kErrorAuthenticationDefault)
            } else if facebookResult.isCancelled {
                print("Facebook login was cancelled.")
            } else {
                Utility.showIndicatorForView(self.view)
                let accessToken = FBSDKAccessToken.currentAccessToken().tokenString
                rootRef.authWithOAuthProvider("facebook", token: accessToken,
                    withCompletionBlock: { error, authData in
                        if error != nil {
                            Utility.showToastWithMessage(kErrorAuthenticationDefault)
                            Utility.removeIndicatorForView(self.view)
                        } else {
                            print("Logged in! \(authData)")
                            userRef.childByAppendingPath(authData.uid).observeSingleEventOfType(.Value ,withBlock: {
                                snapshot in
                                // user has exits
                                print(snapshot.value)
                                if  !(snapshot.value is NSNull) {
                                    if let data = snapshot.value as? NSDictionary {
                                        //self.setCurrentUser(snapshot.key, data: data)
                                        if let user = User.getUserWithID(appDelegate.managedObjectContext, uID: snapshot.key) {
                                            user.reloadData(snapshot.key, data: data)
                                        } else {
                                            currentUser = User.createInManagedObjectContext(appDelegate.managedObjectContext, uid: snapshot.key, data: data)
                                        }
                                        Utility.openAuthenticated(self)
                                    }
                                }
                                    //new user
                                else {
                                    var email = ""
                                    var name = ""
                                    if let e = authData.providerData["email"] as? String {
                                        email = e
                                    }
                                    if let n = authData.providerData["displayName"] as? String {
                                        name = n
                                    }
                                    User.createNewUser(authData.uid, email: email, name: name)
                                    Utility.removeIndicatorForView(self.view)
                                    //self.signUp()
                                }
                                }, withCancelBlock: { error in
                                    Utility.removeIndicatorForView(self.view)
                            })
                        }
                })
            }
        })
    }
    
    @IBAction func googleLoginAction(sender: AnyObject) {
        authenticateWithGoogle(sender as! UIButton)
    }

    // private func
    func createNewUser(uid: String,email: String, name: String) {
        let newUser = [
            "email": email,
            "name": name,
            ]
        userRef.childByAppendingPath(uid).setValue(newUser)
        Utility.openAuthenticated(self)
    }
    
    //MARK: Google login
    //MARK: Google Login
    func signInWillDispatch(signIn: GIDSignIn!, error: NSError!) {
        // myActivityIndicator.stopAnimating()
    }
    
    // Present a view that prompts the user to sign in with Google
    func signIn(signIn: GIDSignIn!,
                presentViewController viewController: UIViewController!) {
        self.presentViewController(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    // Wire up to a button tap
    func authenticateWithGoogle(sender: UIButton) {
        GIDSignIn.sharedInstance().signIn()
    }
    func signOut() {
        GIDSignIn.sharedInstance().signOut()
        rootRef.unauth()
    }
    // Implement the required GIDSignInDelegate methods
    func signIn(signIn: GIDSignIn!, didSignInForUser user: GIDGoogleUser!,
                withError error: NSError!) {
        if (error == nil) {
            // Auth with Firebase
            Utility.showIndicatorForView(self.view)
            rootRef.authWithOAuthProvider("google", token: user.authentication.accessToken, withCompletionBlock: { (error, authData) in
                // User is logged in!
                if error != nil {
                    Utility.showToastWithMessage(kErrorAuthenticationDefault)
                    Utility.removeIndicatorForView(self.view)
                } else {
                    print("Logged in! \(authData)")
                    userRef.childByAppendingPath(authData.uid).observeSingleEventOfType(.Value ,withBlock: {
                        snapshot in
                        // user has exits
                        print(snapshot.value)
                        if  !(snapshot.value is NSNull) {
                            if let data = snapshot.value as? NSDictionary {
                                //self.setCurrentUser(snapshot.key, data: data)
                                if let user = User.getUserWithID(appDelegate.managedObjectContext, uID: snapshot.key) {
                                    user.reloadData(snapshot.key, data: data)
                                } else {
                                    currentUser = User.createInManagedObjectContext(appDelegate.managedObjectContext, uid: snapshot.key, data: data)
                                }
                                Utility.openAuthenticated(self)
                            }
                        }
                            //new user
                        else {
                            var email = ""
                            var name = ""
                            if let e = authData.providerData["email"] as? String {
                                email = e
                            }
                            if let n = authData.providerData["displayName"] as? String {
                                name = n
                            }
                            User.createNewUser(authData.uid, email: email, name: name)
                            Utility.removeIndicatorForView(self.view)
                            //self.signUp()
                        }
                        }, withCancelBlock: { error in
                            Utility.removeIndicatorForView(self.view)
                    })
                }
            })
        } else {
            // Don't assert this error it is commonly returned as nil
            print("\(error.localizedDescription)")
        }
    }
    
    // Implement the required GIDSignInDelegate methods
    // Unauth when disconnected from Google
    func signIn(signIn: GIDSignIn!, didDisconnectWithUser user:GIDGoogleUser!,
                withError error: NSError!) {
        rootRef.unauth();
    }
    
    
    // MARK: Button's actions
    
}

