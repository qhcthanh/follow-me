//
//  TripInfo.swift
//  FollowMe
//
//  Created by Developer on 5/1/16.
//  Copyright © 2016 The Simple Studio. All rights reserved.
//

import Foundation
import GoogleMaps
import Firebase

class TripInfo: NSObject {
    var name: String = ""
    var fromPlace: PlaceInfo?
    var toPlace: PlaceInfo?
    var friends = [FriendInfo]()
    var created :NSDate? = NSDate()
    var key = ""
    var userID: String?
    var userName: String?
    var status = 0
    
    class func fromDictionary(tripDic: NSDictionary) -> TripInfo? {
        let apm = TripInfo()
        if let key = tripDic["key"] as? String {
            apm.key = key
        } else {
            return nil
        }
        if let name = tripDic["name"] as? String {
            apm.name = name
        }
        if let created = tripDic["created"] as? String {
            //2016-03-24 18:28:25 +0000"
            let format = NSDateFormatter()
            format.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ"
            apm.created = format.dateFromString(created)
        }
        if let stt = tripDic["status"] as? Int {
            apm.status = stt
        }
        if let userID = tripDic["userID"] as? String {
            apm.userID = userID
        }
        
        if let fromPlace = tripDic["fromPlace"] as? NSDictionary {
            apm.fromPlace = PlaceInfo.fromDictionary(fromPlace)
        }
        
        if let toPlace = tripDic["toPlace"] as? NSDictionary {
            apm.toPlace = PlaceInfo.fromDictionary(toPlace)
        }
        if let inviteFr = tripDic["friends"] as? NSDictionary {
            for k in inviteFr.allKeys {
                if let key = k as? String {
                    if let value = inviteFr[key] as? NSDictionary {
                        if let e = value["email"] as? String,
                            let name = value["name"] as? String, let p = value["phone"] as? String {
                            apm.friends.append(FriendInfo(uid: key, name: name, email: e, phone: p))
                        }
                    }
                }
            }
        }
        if let userN = tripDic["userName"] as? String {
            apm.userName = userN
        }
        return apm
    }
    
    class func loadTripWithKey(key:String,completionBlock: (TripInfo) -> Void) {
//        if let app = Trip.getTripsFromKeys(appDelegate.managedObjectContext, key: key) {
//            completionBlock(app.toTripInfo())
//        }
        tripsRef.childByAppendingPath(key).observeEventType(.Value, withBlock: {
            snapshot in
            let apm = TripInfo()
            if let tripDic = snapshot.value as? NSDictionary {
                apm.key = key
                if let name = tripDic["name"] as? String {
                    apm.name = name
                }
                if let created = tripDic["created"] as? String {
                    //2016-03-24 18:28:25 +0000"
                    let format = NSDateFormatter()
                    format.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ"
                    apm.created = format.dateFromString(created)
                }
                if let stt = tripDic["status"] as? Int {
                    apm.status = stt
                }
                if let userID = tripDic["userID"] as? String {
                    apm.userID = userID
                }
                
                if let fromPlace = tripDic["fromPlace"] as? NSDictionary {
                    apm.fromPlace = PlaceInfo.fromDictionary(fromPlace)
                }

                if let toPlace = tripDic["toPlace"] as? NSDictionary {
                    apm.toPlace = PlaceInfo.fromDictionary(toPlace)
                }
                if let inviteFr = tripDic["friends"] as? NSDictionary {
                    for k in inviteFr.allKeys {
                        if let key = k as? String {
                            if let value = inviteFr[key] as? NSDictionary {
                                if let e = value["email"] as? String,
                                    let name = value["name"] as? String, let p = value["phone"] as? String {
                                    apm.friends.append(FriendInfo(uid: key, name: name, email: e, phone: p))
                                }
                            }
                        }
                    }
                }
                
                if let userN = tripDic["userName"] as? String {
                    apm.userName = userN
                }
                
                if let _ = try? appDelegate.managedObjectContext.save() {
                    // Save ok
                    print("Coredata save OK")
                }
                else {
                    // Save error
                    print("Coredata save Error")
                }
                completionBlock(apm)
            }
        })
    }
    
    func toDictionary() -> NSDictionary? {
        if let user = currentUser, let fromPlace = fromPlace, let toPlace = toPlace where name.count() > 4 && friends.count > 0 {
            let fromDic = fromPlace.toDictionary()
            let toDic = toPlace.toDictionary()
            let friendsDiction = FriendInfo.getListDictionary(friends)
            let newTrip = [
                "name": name,
                "toPlace": toDic,
                "fromPlace": fromDic,
                "friends": friendsDiction,
                "created": created!.description,
                "userID": userID!,
                "status": status,
                "key": key,
                "userName": user.name
            ]
            return newTrip
        }
        return nil
    }
    
    func syncToFirebase() -> Bool {
        if let user = currentUser, let fromPlace = fromPlace, let toPlace = toPlace where name.count() > 4 && friends.count > 0 {
            let fromDic = fromPlace.toDictionary()
            let toDic = toPlace.toDictionary()
            let friendsDiction = FriendInfo.getListDictionary(friends)
            var tripRef = tripsRef
            if key != "" {
                tripRef = tripRef.childByAppendingPath(key)
            } else {
                tripRef = tripRef.childByAutoId()
                key = tripRef.key
                userID = user.uid
            }
            // sync Trip Table
            let newTrip = [
                "name": name,
                "toPlace": toDic,
                "fromPlace": fromDic,
                "friends": friendsDiction,
                "created": created!.description,
                "userID": userID!,
                "status": status,
                "key": key,
                "userName": user.name
            ]
            tripRef.setValue(newTrip)
            //sync to user
                //sync to me
                userRef.childByAppendingPath(user.uid).childByAppendingPath("trip").setValue(newTrip)
            //sync request trip
            for fr in friends {
                userRef.childByAppendingPath(fr.uid).childByAppendingPath("tripRequest").childByAppendingPath(self.key).setValue(newTrip)
            }
            
            return true
        }
        return false
    }
}