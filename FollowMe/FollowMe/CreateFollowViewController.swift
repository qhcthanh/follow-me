//
//  CreateFollowViewController.swift
//  FollowMe
//
//  Created by Developer on 4/20/16.
//  Copyright © 2016 The Simple Studio. All rights reserved.
//

import UIKit
import GoogleMaps

class CreateFollowViewController: UIViewController, GMSMapViewDelegate, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tripNameTextField: UITextField!
    @IBOutlet weak var fromLocationButton: UIButton!
    @IBOutlet weak var toLocationButton: UIButton!
    @IBOutlet weak var friendsTableView: UITableView!
    @IBOutlet weak var detailMapView: GMSMapView!
    
    var placesClient: GMSPlacesClient = GMSPlacesClient()
    var placePicker: GMSPlacePicker?
    var tripInfo = TripInfo()
    var markerTo = GMSMarker()
    var markerFrom = GMSMarker()
    var friends = [FriendInfo]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.appendFriend(_:)), name: "ADDFRIENDTRIP", object: nil)
        let tapGetestue = UITapGestureRecognizer(target: self, action: #selector(endEditing))
        tapGetestue.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapGetestue)
        detailMapView.addGestureRecognizer(tapGetestue)
        friendsTableView.addGestureRecognizer(tapGetestue)
        
        self.markerTo.map = self.detailMapView
        self.markerTo.icon = UIImage(named: "Marker")
        self.markerTo.flat = true
        self.markerFrom.map = self.detailMapView
        self.markerFrom.icon = UIImage(named: "Marker Filled")
        self.markerFrom.flat = true
        
        detailMapView.myLocationEnabled = true
        
    }
    
    func appendFriend(notification: NSNotification) {
        if notification.name == "ADDFRIENDTRIP" {
            if let info = notification.userInfo, let appFriends =  info["friends"] as? [FriendInfo] {
                for fr in appFriends {
                    self.friends.append(fr)
                }
                friendsTableView.reloadData()
                tripInfo.friends = appFriends
            }
        }
    }
    
    func endEditing() {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func updateDetailMapView() {
        if let fromPlace = tripInfo.fromPlace, let toPlace = tripInfo.toPlace,
            let fC = fromPlace.coordinate,let tC = toPlace.coordinate {
            let bounds = GMSCoordinateBounds(coordinate: fC, coordinate: tC)
            let camera = GMSCameraUpdate.fitBounds(bounds)
            detailMapView.moveCamera(camera)
        } else {
            if let currenLocation = currentUser?.currentLocation {
                let camera = GMSCameraPosition.cameraWithTarget(currenLocation, zoom: 16)
                detailMapView.camera = camera
            }
        }
    }
    
    @IBAction func cancelAction(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func doneAction(sender: AnyObject) {
        if let name = tripNameTextField.text {
            tripInfo.name = name
            if tripInfo.syncToFirebase() {
                NSNotificationCenter.defaultCenter().postNotificationName("NEWTRIPADD", object: nil, userInfo: ["trip":tripInfo])
                self.navigationController?.popViewControllerAnimated(true)
            } else {
                Utility.showToastWithMessage("Có lỗi xãy ra, vui lòng thử lại.")
            }
        } else {
            Utility.showToastWithMessage("Tên chuyến đi không được rỗng")
        }
        
        
    }
    
    @IBAction func detailAction(sender: AnyObject) {
        detailMapView.hidden = !detailMapView.hidden
        updateDetailMapView()
    }
    
    @IBAction func addFriendAction(sender: AnyObject) {
        endEditing()
    }
    
    @IBAction func fromLocationAction(sender: AnyObject) {
        pickPlace(sender as! UIButton)
    }
    
    @IBAction func toLocationAction(sender: AnyObject) {
        pickPlace(sender as! UIButton)
    }
    
    func pickPlace(sender: UIButton) {
        endEditing()
        var center = CLLocationCoordinate2DMake(37.788204, -122.411937)
        if let location = currentUser?.currentLocation {
            center = location
        }
        let northEast = CLLocationCoordinate2DMake(center.latitude + 0.001, center.longitude + 0.001)
        let southWest = CLLocationCoordinate2DMake(center.latitude - 0.001, center.longitude - 0.001)
        let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
        let config = GMSPlacePickerConfig(viewport: viewport)
        self.placePicker = GMSPlacePicker(config: config)
        self.placePicker!.pickPlaceWithCallback({ (place: GMSPlace?, error: NSError?) -> Void in
            if let error = error {
                print("Pick Place error: \(error.localizedDescription)")
                return
            }
            if let place = place {
                var add = "None"
                if let address = place.formattedAddress {
                    add = address
                }
                
                let place = PlaceInfo(name: place.name, address: add, placeID: place.placeID, coordinate: place.coordinate)
                sender.setTitle(place.name, forState: .Normal)
                if sender == self.fromLocationButton {
                    self.tripInfo.fromPlace = place
                    self.markerTo.position = place.coordinate!
                    self.markerTo.title = place.name
                    self.markerTo.snippet = add
                    
                } else {
                    self.tripInfo.toPlace = place
                    self.markerFrom.position = place.coordinate!
                    self.markerFrom.title = place.name
                    self.markerFrom.snippet = add
                }
                self.updateDetailMapView()
            }
            
        })
    }
    
    
    //MARK: UITableView DataSource
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return friends.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("FriendCell") as! FriendCell
        cell.cleanUI()
        cell.friend = friends[indexPath.row]
        cell.renderUI()
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60
    }
}
