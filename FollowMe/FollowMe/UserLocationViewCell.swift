//
//  UserLocationViewCell.swift
//  FollowMe
//
//  Created by Developer on 5/3/16.
//  Copyright © 2016 The Simple Studio. All rights reserved.
//

import UIKit
import GoogleMaps

class UserLocationViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var stateImageView: UIImageView!
    @IBOutlet weak var userImageView: UIImageView!
    
    var friend: FriendInfo?
    var handle: UInt?
    
    func cleanUI() {
        titleLabel.text = "Tên bạn"
        subTitleLabel.text = "Đang cập nhật....."
        userImageView.image = UIImage(named: "Collaborator Male-50")
        stateImageView.image = nil
    }
    
    func renderUI() {
        if let friend = friend {
            titleLabel.text = friend.name
            if let img =  friend.image {
                userImageView.image = img
            } else {
                userImageView.image = UIImage(named: "Collaborator Male-50")
                updateLocation(friend.uid)
            }
            //State
            
        }
    }
    
    func updateLocation(uid: String) {
        handle = userRef.childByAppendingPath(uid).childByAppendingPath("location").observeEventType(.Value, withBlock: {
            snapshot in
            if !(snapshot.value is NSNull) {
                if let dic = snapshot.value as? NSDictionary,
                    let lat = dic["latitude"] as? Double,
                    let long = dic["longitude"] as? Double,
                    let time = dic["time"] as? String {
                        self.friend?.location = CLLocationCoordinate2D(latitude: lat, longitude: long)
                        let format = NSDateFormatter()
                        format.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ"
                        self.friend?.updateTime = format.dateFromString(time)
                    self.subTitleLabel.text = "\(lat)-\(long)-\(time)"
                }
            }
        })
    }
    
    deinit {
        if let handle = handle {
            userRef.removeObserverWithHandle(handle)
        }
    }
}
