//
//  FollowsControllerViewController.swift
//  FollowMe
//
//  Created by Developer on 4/20/16.
//  Copyright © 2016 The Simple Studio. All rights reserved.
//

import UIKit

class FollowsControllerViewController: UIViewController {
    @IBOutlet weak var followSegment: UISegmentedControl!
    @IBOutlet weak var requestView: UIView!
    @IBOutlet weak var historyView: UIView!
    @IBOutlet weak var friendView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        requestView.hidden = true
        historyView.hidden = true
        friendView.hidden = false
    }
    @IBAction func followSegmentAction(sender: AnyObject) {
        switch followSegment.selectedSegmentIndex {
        case 0:
            requestView.hidden = true
            historyView.hidden = true
            friendView.hidden = false
        case 1:
            requestView.hidden = true
            historyView.hidden = false
            friendView.hidden = true
        case 2:
            requestView.hidden = false
            historyView.hidden = true
            friendView.hidden = true
        default:
            requestView.hidden = true
            historyView.hidden = true
            friendView.hidden = false
        }
    }
}
