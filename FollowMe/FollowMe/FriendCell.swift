//
//  FriendCell.swift
//  FollowMe
//
//  Created by Developer on 4/19/16.
//  Copyright © 2016 The Simple Studio. All rights reserved.
//

import UIKit

class FriendCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var subLabel: UILabel!
    
    var friend: FriendInfo?
    
    func cleanUI() {
        nameLabel.text = ""
        subLabel.text = ""
    }
    
    func renderUI() {
        nameLabel.text = friend?.name
        subLabel.text = friend?.phone
    }
}
