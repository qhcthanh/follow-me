//
//  ContactInfo.swift
//  meetmi
//
//  Created by Developer on 4/11/16.
//  Copyright © 2016 The Simple Studio. All rights reserved.
//

import Foundation

class ContactInfo: NSObject, NSCoding {
    var name: String
    var phone: String
    var image: UIImage?
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(self.name,forKey: "name")
        aCoder.encodeObject(self.phone,forKey: "phone")
        aCoder.encodeObject(self.image, forKey: "image")
    }
    
    required convenience init?(coder decoder: NSCoder) {
        guard
            let name = decoder.decodeObjectForKey("name") as? String,
            let phone = decoder.decodeObjectForKey("phone") as? String
            else { return nil }
        
        self.init (
            name: name,
            phone: phone
        )
        if let image = decoder.decodeObjectForKey("image") as? UIImage {
            self.image = image
        }
    }
    
    required init(name: String, phone: String) {
        self.name = name
        self.phone = phone
    }
    
    convenience init(name: String, phone: String, image: UIImage?) {
        self.init(name: name,phone: phone)
        self.image = image
    }
}