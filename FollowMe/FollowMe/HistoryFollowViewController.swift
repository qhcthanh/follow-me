//
//  HistoryFollowViewController.swift
//  FollowMe
//
//  Created by Developer on 4/20/16.
//  Copyright © 2016 The Simple Studio. All rights reserved.
//

import UIKit
import Firebase
class HistoryFollowViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, HistoryCellProtocol {
    
    //MARK: UI Element
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: private property
    var tripHistory = [TripInfo]()
    
    //MARK: Load
    override func viewDidLoad() {
        super.viewDidLoad()
        self.observeTripHistory()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        
    }
    
    func observeTripHistory() {
        if let user = currentUser {
            userRef.childByAppendingPath(user.uid).childByAppendingPath("tripHistory").observeEventType(.Value, withBlock: {
                snapshot in
                self.tripHistory.removeAll()
                for ele in snapshot.children {
                    let eleData = ele as? FDataSnapshot
                    if let dic = eleData?.value as? NSDictionary {
                        if let data = TripInfo.fromDictionary(dic) {
                            self.tripHistory.append(data)
                        }
                    }
                }
                self.tableView.reloadData()
            })
        }
    }
    
    //MARK: UITableViewDataSource + UITableViewDelegate
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tripHistory.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("TripCell") as! TripCell
        cell.cleanUI()
        cell.trip = tripHistory[indexPath.row]
        cell.renderUI()
        cell.delegateHistory = self
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 85
    }
    
    func infoTripAction(trip: TripInfo?) {
        
    }
    
    func deleteTripAction(trip: TripInfo?) {
        
    }

}
