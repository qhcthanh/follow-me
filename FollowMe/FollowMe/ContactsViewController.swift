//
//  ContactsViewController.swift
//  FollowMe
//
//  Created by Developer on 4/20/16.
//  Copyright © 2016 The Simple Studio. All rights reserved.
//

import UIKit
import SwiftAddressBook
import AddressBook
import SystemConfiguration
import Foundation

class ContactsViewController: UIViewController {

    @IBOutlet weak var segmentController: UISegmentedControl!
    @IBOutlet var friendViewController: UIView!
    @IBOutlet weak var contactViewController: UIView!
    
    var rightButtonFriend: UIBarButtonItem!
    var rightButtonContact: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.contactViewController.hidden = true
        self.rightButtonFriend = self.navigationItem.rightBarButtonItem
        rightButtonContact = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Refresh, target: self, action: #selector(self.loadContact))
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func loadContact() {
        Utility.showIndicatorForView(self.view)
        SwiftAddressBook.requestAccessWithCompletion { (success :Bool, _ :CFError?) -> Void in
            if success {
                people = swiftAddressBook?.allPeople
                if  let people = people {
                    //access variables on any entry of allPeople array just like always
                    var contacts = [ContactInfo]()
                    for person in people {
                        let numbers = person.phoneNumbers
                        var name: String = ""
                        if let fName = person.firstName {
                            name += fName
                        }
                        if let lName = person.lastName {
                            name += " " + lName
                        }
                        //the value entry of the multivalue struct contains the data
                        if let number = numbers, let value = number.first {
                            let isFriend : Bool = false
                            let phone = value.value.trim()
                            if !isFriend {
                                contacts.append(ContactInfo(name: name, phone: phone, image: person.image))
                            }
                        }
                    }
//                    self.contactList = contactsX
                    NSNotificationCenter.defaultCenter().postNotificationName("LOADCONTACT", object: nil, userInfo: ["contact":contacts])
                    // sync contact to firebase
                    if let user = currentUser {
                        let ref = userRef.childByAppendingPath(user.uid).childByAppendingPath("contact")
                        // convert contacts -> dictionary
                        if let arr = Utility.getArrayContact(contacts) {
                            ref.setValue(arr)
                        }
                        //update to coredata
                        user.contactArray = contacts
                        user.contact = NSKeyedArchiver.archivedDataWithRootObject(contacts)
                        if let _ = try? appDelegate.managedObjectContext.save() {
                            print("save coredate complete")
                        } else {
                            print("save coredata fail")
                        }
                    }
                    
                    dispatch_async(dispatch_get_main_queue(),{
                       // self.friendsTableView.reloadSections(NSIndexSet(index: 1), withRowAnimation: UITableViewRowAnimation.Automatic)
                    })
                }
            }
            Utility.removeIndicatorForView(self.view)
        }

    }
    
    @IBAction func segmentAction(sender: AnyObject) {
        if sender.selectedSegmentIndex == 0 {
            self.friendViewController.hidden = false
            self.contactViewController.hidden = true
            self.navigationItem.rightBarButtonItem = rightButtonFriend
        } else {
            self.friendViewController.hidden = true
            self.contactViewController.hidden = false
            self.navigationItem.rightBarButtonItem = rightButtonContact
        }
    }
    
    @IBAction func addFriendAction(sender: AnyObject) {
    }
    
    
}
