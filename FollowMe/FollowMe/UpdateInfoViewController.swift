//
//  UpdateInfoViewController.swift
//  FollowMe
//
//  Created by Developer on 4/22/16.
//  Copyright © 2016 The Simple Studio. All rights reserved.
//

import UIKit

class UpdateInfoViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let user = currentUser {
            nameTextField.text = user.name
            emailTextField.text = user.email
            phoneTextField.text = user.phone
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func doneAction(sender: AnyObject) {
        if let user = currentUser, let phone = phoneTextField.text {
            print(nameTextField.text)
            print(emailTextField.text)
            if phone.isNumberPhone() && nameTextField.text != "" && emailTextField.text != ""  {
                
                rootRef.queryOrderedByChild("phone").queryEqualToValue(phone)
                    .observeSingleEventOfType(.Value, withBlock: { result in
                        if !(result.value is NSNull)
                        {
                            Utility.showToastWithMessage("Phone belongs to an existing account!")
                        }
                        else
                        {
                            rootRef.childByAppendingPath("users").childByAppendingPath(user.uid).childByAppendingPath("name").setValue(self.nameTextField.text!)
                            rootRef.childByAppendingPath("users").childByAppendingPath(user.uid).childByAppendingPath("phone").setValue(phone)
                            
                            user.phone = phone
                            user.name = self.nameTextField.text!
                            user.email = self.emailTextField.text!
                            if let _ = try? appDelegate.managedObjectContext.save() {}
                            
                            self.dismissViewControllerAnimated(true, completion: nil)
                        }
                    })
                
            } else {
                Utility.showToastWithMessage("Email, Name and Phone could not be empty")
            }
        } else {
            Utility.showToastWithMessage("Error! Please try again.")
        }
    
        
    }
}
