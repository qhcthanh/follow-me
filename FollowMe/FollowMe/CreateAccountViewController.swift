
//
//  CreateAccountViewController.swift
//  FollowMe
//
//  Created by Developer on 4/13/16.
//  Copyright © 2016 The Simple Studio. All rights reserved.
//

import UIKit
import Firebase
class CreateAccountViewController: UIViewController {

    @IBOutlet weak var textAccount: UITextField!
    @IBOutlet weak var textPass: UITextField!
    @IBOutlet weak var textRetypePass: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(SignInViewController.endEditing))
        tapGesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapGesture)
    }
    
    func endEditing() {
        self.view.endEditing(true)
    }
    
    @IBAction func touchSignUp(sender: AnyObject) {
        if textAccount.text == "" || textPass.text == "" || textRetypePass.text == "" || textPass.text != textRetypePass.text
        {
            
        }
        else
        {
            Utility.showIndicatorForView(self.view)
            rootRef.createUser(textAccount.text, password: textPass.text, withValueCompletionBlock:
                { error, result in
                    if error != nil {
                        // There was an error creating the account
                        if let errorCode = FAuthenticationError(rawValue: error.code) {
                            switch (errorCode) {
                            case .UserDoesNotExist:
                                Utility.showToastWithMessage(kErrorSignInUserDoesNotExist)
                            case .InvalidEmail:
                                Utility.showToastWithMessage(kErrorSignInInvalidEmail)
                            case .InvalidPassword:
                                Utility.showToastWithMessage(kErrorSignInInvalidPassword)
                            case .NetworkError:
                                Utility.showToastWithMessage(kErrorNetwork)
                            default:
                                Utility.showToastWithMessage(kErrorAuthenticationDefault)
                            }
                        }
                        Utility.removeIndicatorForView(self.view)
                    } else {
                        let uid = result["uid"] as? String
                        print("Successfully created user account with uid: \(uid)")
                        if let textAccount = self.textAccount.text {
                            rootRef.childByAppendingPath("users").childByAppendingPath(uid!).setValue([
                            "email":  "\(textAccount)"
                            ])
                            rootRef.authUser(self.textAccount.text, password: self.textPass.text, withCompletionBlock: {
                                error, authData in
                                if error != nil {
                                    if let errorCode = FAuthenticationError(rawValue: error.code) {
                                        switch (errorCode) {
                                        case .NetworkError:
                                            Utility.showToastWithMessage(kErrorNetwork)
                                        default:
                                            Utility.showToastWithMessage(kErrorSignUpCantCreateUser)
                                        }

                                    }
                                    // There was an error logging in to this account
                                    Utility.showToastWithMessage("Email or Password is not corrected")
                                } else {
                                    // Load data here
                                    User.createNewUser(authData.uid, email: textAccount, name: UIDevice.currentDevice().name)
                                    currentUser = User.getUserWithID(appDelegate.managedObjectContext, uID: authData.uid)
                                    //Open Authenticated
                                    Utility.openAuthenticated(self)
                                }
                                Utility.removeIndicatorForView(self.view)

                            })
                            // load user data here
                        }
                    }
            })

            
        }
        
        
    }
}
