//
//  FriendInfo.swift
//  FollowMe
//
//  Created by Developer on 5/1/16.
//  Copyright © 2016 The Simple Studio. All rights reserved.
//

import UIKit
import GoogleMaps

class FriendInfo: NSObject, NSCoding {
    var uid: String
    var name: String
    var email: String
    var phone: String
    var image: UIImage?
    var location: CLLocationCoordinate2D?
    var updateTime: NSDate?

    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(self.email,forKey: "email")
        aCoder.encodeObject(self.name,forKey: "name")
        aCoder.encodeObject(self.uid, forKey: "uid")
        aCoder.encodeObject(self.phone, forKey: "phone")
        aCoder.encodeObject(self.image,forKey: "image")
    }
    required convenience init?(coder decoder: NSCoder) {
        guard let email = decoder.decodeObjectForKey("email") as? String,
            let name = decoder.decodeObjectForKey("name") as? String ,
            let uid = decoder.decodeObjectForKey("uid") as? String,
            let phone = decoder.decodeObjectForKey("phone") as? String
            else { return nil }
        self.init(
            uid: uid,
            name: name,
            email: email,
            phone: phone
        )
        if let image = decoder.decodeObjectForKey("image") as? UIImage {
            self.image = image
        }
    }
    
    required init(uid: String, name: String, email: String, phone: String) {
        self.uid = uid
        self.name = name
        self.email = email
        self.phone = phone
    }
    
    class func fromDictionary(uid: String, data: NSDictionary) -> FriendInfo? {
        if let name = data["name"] as? String,
            let phone = data["phone"] as? String,
            let email = data["email"] as? String {
            return FriendInfo(uid: uid, name: name, email: email, phone: phone)
        }
        return nil
    }
    
    class func getListDictionary(friends: [FriendInfo]) -> NSDictionary {
        var dict: Dictionary<String,NSDictionary> = Dictionary<String,NSDictionary>()
        for fr in friends {
            dict[fr.uid] = [ "email": fr.email, "name": fr.name, "phone": fr.phone]
        }
        return dict
    }
    
    func deleteFriend() {
        if let id = currentUser?.uid {
            // delete in currentUser
            userRef.childByAppendingPath(id)
                .childByAppendingPath("friends")
                .childByAppendingPath(self.uid).removeValue()
            // delete target user
            userRef.childByAppendingPath(self.uid)
                .childByAppendingPath("friends")
                .childByAppendingPath(id).removeValue()
            //delete in coredata
            if let friends = currentUser?.friendsDictionary {
                if var dict = friends as? Dictionary<String, NSDictionary> {
                    dict.removeValueForKey(self.uid)
                    currentUser?.friendsDictionary = dict
                    
                }
                
            }
        }
    }
    
    class func loadToCurrentUser(friends:[FriendInfo]) {
        if let user = currentUser {
            var dict: Dictionary<String,NSDictionary> = Dictionary<String,NSDictionary>()
            for friend in friends {
                dict[friend.uid] = [
                    "email": friend.email,
                    "name": friend.name,
                    "phone": friend.phone
                ]
            }
            user.friendsDictionary = dict
            user.friends = NSKeyedArchiver.archivedDataWithRootObject(user.friendsDictionary!)
        }
    }
    
    class func getCurrentFriends() -> [FriendInfo]? {
        if let friendDict = currentUser?.friendsDictionary {
            var friends: [FriendInfo] = [FriendInfo]()
            for key in friendDict.allKeys {
                if let key = key as? String {
                    if let value = friendDict[key] as? NSDictionary {
                        var email = ""
                        var name = ""
                        var phone = ""
                        if let e = value["email"] as? String {
                            email = e
                        }
                        if let n = value["name"] as? String {
                            name = n
                        }
                        if let p = value["phone"] as? String {
                            phone = p
                        }
                        friends.append(FriendInfo(uid: key, name: name, email: email, phone: phone))
                    }
                }
            }
            return friends
        }
        return nil
    }

    
    func toDictionary() -> NSDictionary {
        let data = [
            "name": name,
            "email": email,
            "phone": phone
        ]
        return data
    }
}
