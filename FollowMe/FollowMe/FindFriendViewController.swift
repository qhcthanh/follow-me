//
//  FindFriendViewController.swift
//  FollowMe
//
//  Created by Developer on 5/2/16.
//  Copyright © 2016 The Simple Studio. All rights reserved.
//

import UIKit

class FindFriendViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var friendsTableView: UITableView!
    var friends = [FriendInfo]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGest = UITapGestureRecognizer(target: self, action: #selector(self.endEditing))
        tapGest.cancelsTouchesInView = false
        friendsTableView.addGestureRecognizer(tapGest)
        if let friends = FriendInfo.getCurrentFriends() {
            self.friends = friends
            friendsTableView.reloadData()
        }
    }
    
    func endEditing() {
        self.view.endEditing(true)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
    }
    
    @IBAction func backAction(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func doneAction(sender: AnyObject) {
        if let indexPaths = friendsTableView.indexPathsForSelectedRows
            where !indexPaths.isEmpty {
            var temp = [FriendInfo]()
            for index in indexPaths {
                temp.append(friends[index.row])
            }
            NSNotificationCenter.defaultCenter().postNotificationName("ADDFRIENDTRIP", object: nil, userInfo: ["friends":temp])
            self.navigationController?.popViewControllerAnimated(true)
        } else {
            Utility.showToastWithMessage("Cần phải chọn ít nhất một bạn")
        }
    }
    
    //MARK: UITableView
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return friends.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("FriendCell") as! FriendCell
        cell.cleanUI()
        cell.friend = friends[indexPath.row]
        cell.renderUI()
        return cell
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField.text == "" {
            self.view.endEditing(true)
        }
        return true
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60
    }

}
