//
//  FollowsViewController.swift
//  FollowMe
//
//  Created by Developer on 4/19/16.
//  Copyright © 2016 The Simple Studio. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation


class FollowsViewController: UIViewController, GMSMapViewDelegate, CLLocationManagerDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var friendFollowTableView: UITableView!
    var locationManager = CLLocationManager()
    var friends = [FriendInfo]()
    @IBOutlet weak var mapView: GMSMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.distanceFilter = 100
        locationManager.startUpdatingLocation()
        locationManager.requestAlwaysAuthorization()
        mapView.myLocationEnabled = true
        if let location = mapView.myLocation {
            print("\(location.coordinate.latitude) - \(location.coordinate.longitude)")
        }
        if let user = currentUser {
            print(user.email)
            print(user.name)
            print(user.uid)
        }
        
        if let friends = FriendInfo.getCurrentFriends() {
            self.friends = friends
        }
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.updateCurrentLocation), name: "FollowsViewController_Update", object: nil)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func addFollowAction(sender: AnyObject) {
        mapView.hidden = !mapView.hidden
    }
    
    //MARK: Table View
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("UserLocationCell") as! UserLocationViewCell
        cell.cleanUI()
        cell.titleLabel.text = friends[indexPath.row].name
        cell.subTitleLabel.text = "Đang cập nhật"
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return friends.count
    }
    
    //MARK: Location
//    func didTapMyLocationButtonForMapView(mapView: GMSMapView) -> Bool {
//        return true
//    }
//    
//    func locationManagerDidPauseLocationUpdates(manager: CLLocationManager) {
//        
//    }
//    
//    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        if let currentLocation = locations.first {
//            let coordinate = CLLocationCoordinate2D(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
//            mapView.camera = GMSCameraPosition.cameraWithTarget(coordinate, zoom: 16)
//            currentUser?.currentLocation = coordinate
//        }
//        
//    }
//    
//    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
//        print(error.description)
//    }
    
    func updateCurrentLocation(notification: NSNotification) {
        if let user = currentUser where notification.name ==  "FollowsViewController_Update" {
            let coordinate = user.currentLocation
            self.mapView.camera = GMSCameraPosition.cameraWithTarget(coordinate, zoom: 16)
        }
    }
    
}
