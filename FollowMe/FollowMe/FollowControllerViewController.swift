//
//  FollowControllerViewController.swift
//  FollowMe
//
//  Created by Developer on 4/20/16.
//  Copyright © 2016 The Simple Studio. All rights reserved.
//

import UIKit
import CoreLocation

class FollowControllerViewController: UIViewController,CLLocationManagerDelegate {
    
    @IBOutlet weak var followingView: UIView!
    @IBOutlet weak var followView: UIView!
    @IBOutlet weak var addFollowButton: UIBarButtonItem!
    
    var rightBarButtonItem : UIBarButtonItem!
    var locationManager = CLLocationManager()
    override func viewDidLoad() {
        followingView.hidden = true
//        rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add, target: self, action: #selector(addFollowAction))
        rightBarButtonItem = self.navigationItem.rightBarButtonItem
        if let user = currentUser {
            if let _ = user.phone {
                
            } else {
                let initVC  = self.storyboard!.instantiateViewControllerWithIdentifier("UpdateInfoVC")
                self.presentViewController(initVC, animated: true, completion: nil)
                return
            }
        }
        locationManager.delegate = self
        locationManager.distanceFilter = 100
        locationManager.startUpdatingLocation()
        locationManager.requestAlwaysAuthorization()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.newTrip(_:)), name: "NEWTRIPADD", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.outTrip(_:)), name: "outTrip", object: nil)
        observeUserTrip()
    }
    @IBAction func switchFollowAction(sender: AnyObject) {
        updateView()
        if followingView.hidden {
            self.navigationItem.title = "No Trip"
            self.navigationItem.rightBarButtonItem = self.rightBarButtonItem
        } else {
            self.navigationItem.title = "Trip Name"
            self.navigationItem.rightBarButtonItem = nil
        }
    }
    
    @IBAction func addFollowAction(sender: AnyObject) {
        
    }
    
    func observeUserTrip() {
        if let user = currentUser {
            userRef.childByAppendingPath(user.uid).childByAppendingPath("trip").observeSingleEventOfType(.Value, withBlock: {
                snapshot in
                if !(snapshot.value is NSNull) {
                    print("************")
                    print(snapshot.value)
                    if let dic = snapshot.value as? NSDictionary , let trip = TripInfo.fromDictionary(dic) {
                        self.newTrip(NSNotification(name: "NEWTRIPADD", object: nil, userInfo: ["trip":trip]))
                    }
                }
            })
        }
    }
    
    func newTrip(notification: NSNotification) {
        if let user = currentUser, let info = notification.userInfo where notification.name == "NEWTRIPADD" {
            if let trip = info["trip"] as? TripInfo {
                self.navigationItem.title = trip.name
                self.navigationItem.rightBarButtonItem = nil
                followView.hidden = true
                followingView.hidden = false
                NSNotificationCenter.defaultCenter().postNotificationName("FOLLOWEDTRIP", object: nil, userInfo: ["trip":trip])
            }
        }
    }
    
    func outTrip(notification: NSNotification) {
        if let user = currentUser where notification.name == "outTrip" {
            self.navigationItem.title = "No Trip"
            self.navigationItem.rightBarButtonItem = self.rightBarButtonItem
            followView.hidden = false
            followingView.hidden = true
        }
    }
    
    func updateView() {
        followView.hidden = !followView.hidden
        followingView.hidden = !followingView.hidden
    }
    
    //MARK: Location
    func locationManagerDidPauseLocationUpdates(manager: CLLocationManager) {
        
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let currentLocation = locations.first {
            let coordinate = CLLocationCoordinate2D(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
            if let user = currentUser {
                user.currentLocation = coordinate
                userRef.childByAppendingPath(user.uid).childByAppendingPath("location").setValue(
                    [
                        "latitude": coordinate.latitude,
                        "longitude": coordinate.longitude,
                        "time": NSDate().description
                    ])
                //Post to update
                NSNotificationCenter.defaultCenter().postNotificationName("FollowedViewController_Update", object: nil, userInfo: nil)
                NSNotificationCenter.defaultCenter().postNotificationName("FollowsViewController_Update", object: nil, userInfo: nil)
            }
        }
    }
    
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print(error.description)
    }
}
