//
//  Contrainst.swift
//  FollowMe
//
//  Created by Nguyen Thinh on 4/17/16.
//  Copyright © 2016 The Simple Studio. All rights reserved.
//

import Foundation
import Firebase
import SwiftAddressBook

let rootRef = Firebase(url: "https://follow-me.firebaseio.com/")
let userRef = rootRef.childByAppendingPath("users")
let tripsRef = rootRef.childByAppendingPath("trips")
let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
var currentUser: User?
var people : [SwiftAddressBookPerson]?
