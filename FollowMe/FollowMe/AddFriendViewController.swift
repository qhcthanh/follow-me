//
//  AddFriendViewController.swift
//  FollowMe
//
//  Created by Developer on 5/1/16.
//  Copyright © 2016 The Simple Studio. All rights reserved.
//

import UIKit
import Firebase

class AddFriendViewController: UIViewController {

    @IBOutlet weak var typeAddFriendSegment: UISegmentedControl!
    
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var inputImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        inputImageView.image = UIImage(named: "message")
        inputTextField.keyboardType = UIKeyboardType.EmailAddress
        inputTextField.placeholder = "Email bạn tìm kiếm"
        //Phone-50
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @IBAction func addFriendAction(sender: AnyObject) {
        if let user = currentUser, inputText =  inputTextField.text{
            if !(inputText == user.email || inputText == user.phone) {
                Utility.showIndicatorForView(self.view)
                let queryType = typeAddFriendSegment.selectedSegmentIndex == 0 ? "email":"phone"
                rootRef.childByAppendingPath("users").queryOrderedByChild(queryType).queryEqualToValue(inputText).observeSingleEventOfType(.Value, withBlock: {
                    snapshot in
                    print(snapshot.value)
                    Utility.removeIndicatorForView(self.view)
                    if snapshot.value is NSNull {
                        Utility.showToastWithMessage(kErrorCantFind)
                    } else {
                        //Utility.showToastWithMessage("Find that friend with id: \(snapshot.key)")
                        if  let eleData = snapshot.children.allObjects.first as? FDataSnapshot,
                            let dic = eleData.value as? NSDictionary
                        {
                            if let type = dic[queryType] as? String {
                                Utility.showAlertWithMessageOKCancel("Do you want to add \(type) ?", title: "Add friend", sender: self, doneAction: {
                                    () in
                                    let friend = FriendInfo.fromDictionary(eleData.key, data: dic)
                                    userRef.childByAppendingPath(user.uid).childByAppendingPath("friends").childByAppendingPath(eleData.key).setValue(friend?.toDictionary())
                                    userRef.childByAppendingPath(eleData.key).childByAppendingPath("friends").childByAppendingPath(user.uid).setValue(user.toDictionary())
                                    

                                    self.navigationController?.popViewControllerAnimated(true)
                                    }
                                    , cancelAction: nil)
                                return
                        }
                    }
                    }
                })
            }
        } else {
            Utility.showToastWithMessage(kErrorAuthenticationDefault)
        }
    }
    
    @IBAction func typeAddFriendChange(sender: AnyObject) {
        inputTextField.text = ""
        if typeAddFriendSegment.selectedSegmentIndex == 0 {
            inputImageView.image = UIImage(named: "message")
            inputTextField.keyboardType = UIKeyboardType.EmailAddress
        } else {
            inputImageView.image = UIImage(named: "Phone-50")
            inputTextField.keyboardType = UIKeyboardType.PhonePad
            inputTextField.placeholder = "SĐT bạn tìm kiếm"
        }
    }
}
