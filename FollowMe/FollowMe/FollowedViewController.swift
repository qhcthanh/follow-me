//
//  FollowedViewController.swift
//  FollowMe
//
//  Created by Developer on 4/20/16.
//  Copyright © 2016 The Simple Studio. All rights reserved.
//

import UIKit
import GoogleMaps

class FollowedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var detailTripTableView: UITableView!
    var tripInfo = TripInfo()
    var markerTo = GMSMarker()
    var markerFrom = GMSMarker()
    
    func exitAction() {
        if let user = currentUser  {
            let dicTrip = tripInfo.toDictionary()
            userRef.childByAppendingPath(user.uid).childByAppendingPath("trip").removeValue()
            if let dict = dicTrip {
                userRef.childByAppendingPath(user.uid).childByAppendingPath("tripHistory").childByAppendingPath(tripInfo.key).setValue(dict)
            }
            //client
            if  self.tripInfo.userID != user.uid {
                
            } //host
            else {
                //tripsRef.childByAppendingPath(self.tripInfo.key).removeValue()
                for fr in self.tripInfo.friends {
                    userRef.childByAppendingPath(fr.uid).childByAppendingPath("trip").removeValue()
                    if let dict = dicTrip {
                    userRef.childByAppendingPath(fr.uid).childByAppendingPath("tripHistory").childByAppendingPath(tripInfo.key).setValue(dict)
                    }
                }
            }
            //FollowsVC show
            NSNotificationCenter.defaultCenter().postNotificationName("outTrip", object: nil, userInfo: nil)
        }
    }
    
    func loadFloatButton() {
        let fab = KCFloatingActionButton()
        fab.addItem("Thông tin", icon: UIImage(named: "info")!)
        fab.addItem("Cài đặt", icon: UIImage(named: "setting")!)
        fab.addItem("Thoát", icon: UIImage(named: "exit")!, handler: {
            item in
            self.exitAction()
        })
        fab.buttonColor = UIColor.whiteColor()
        self.view.addSubview(fab)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadFloatButton()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.loadTrip(_:)), name: "FOLLOWEDTRIP", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.updateCurrentLocation), name: "FollowedViewController_Update", object: nil)
        
        self.markerTo.map = self.mapView
        self.markerTo.icon = UIImage(named: "Marker")
        self.markerTo.flat = true
        self.markerFrom.map = self.mapView
        self.markerFrom.icon = UIImage(named: "Marker Filled")
        self.markerFrom.flat = true
        mapView.myLocationEnabled = true
    
    }
    
    func updateCurrentLocation(notification: NSNotification) {
        if let user = currentUser where notification.name ==  "FollowedViewController_Update" {
            
        }
    }
    
    func loadTrip(notification: NSNotification) {
        if let info = notification.userInfo where notification.name == "FOLLOWEDTRIP" {
            if let trip = info["trip"] as? TripInfo {
                tripInfo = trip
                loadInfoTrip()
            }
        }
    }
    
    func loadInfoTrip() {
        TripInfo.loadTripWithKey(tripInfo.key, completionBlock: {
            trip in
            if let fromPlace = trip.fromPlace, let toPlace = trip.toPlace,
                let fC = fromPlace.coordinate,let tC = toPlace.coordinate, let user = currentUser {
                self.tripInfo = trip
                //client
                if trip.userID != user.uid {
                    if let index = trip.friends.indexOf({$0.uid == user.uid }) {
                        trip.friends.removeAtIndex(index)
                        trip.friends.append(FriendInfo(uid: trip.userID!, name: trip.userName!, email: "", phone: ""))
                    }
                }
                
                let bounds = GMSCoordinateBounds(coordinate: fC, coordinate: tC)
                let camera = GMSCameraUpdate.fitBounds(bounds)
                
                self.mapView.moveCamera(camera)
                self.markerTo.position = tC
                self.markerFrom.position = fC
                //Draw polyline
                
                //
                self.detailTripTableView.reloadData()
            } else {
                if let currenLocation = currentUser?.currentLocation {
                    let camera = GMSCameraPosition.cameraWithTarget(currenLocation, zoom: 16)
                    self.mapView.camera = camera
                }
            }
        
        })
    }
    
    //UITableView DataSouce 
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tripInfo.friends.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("UserLocationCell") as! UserLocationViewCell
        cell.cleanUI()
        cell.friend = tripInfo.friends[indexPath.row]
        cell.renderUI()
        return cell
    }
}

