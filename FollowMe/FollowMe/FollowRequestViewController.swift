//
//  FollowRequestViewController.swift
//  FollowMe
//
//  Created by Developer on 4/20/16.
//  Copyright © 2016 The Simple Studio. All rights reserved.
//

import UIKit
import Firebase

class FollowRequestViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, TripCellProtocol {
    
    @IBOutlet weak var tableView: UITableView!
    var tripRequest = [TripInfo]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.observerRequestTrip()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: private function
    func observerRequestTrip() {
        if let user = currentUser {
            userRef.childByAppendingPath(user.uid).childByAppendingPath("tripRequest").observeEventType(.Value, withBlock: {
                snapshot in
                self.tripRequest.removeAll()
                for ele in snapshot.children {
                    let eleData = ele as! FDataSnapshot
                    if let dic = eleData.value as? NSDictionary {
                        if let data = TripInfo.fromDictionary(dic) {
                            self.tripRequest.append(data)
                        }
                    }
                }
                self.tableView.reloadData()
            })
        }
    }
    
    
    //MARK: UITableView
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tripRequest.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("TripCell") as! TripCell
        cell.cleanUI()
        cell.trip = tripRequest[indexPath.row]
        cell.delegate = self
        cell.renderUI()
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 85
    }
    
    //MARK: Trip Cell Protocol
    func acceptAction(trip: TripInfo?) {
        if let trip = trip, let user = currentUser {
            userRef.childByAppendingPath(user.uid).childByAppendingPath("tripRequest").childByAppendingPath(trip.key).removeValue()
            userRef.childByAppendingPath(user.uid).childByAppendingPath("trip").setValue(trip.toDictionary()!)
            NSNotificationCenter.defaultCenter().postNotificationName("NEWTRIPADD", object: nil, userInfo: ["trip":trip])
        }
    }
    
    func denyAction(trip: TripInfo?) {
        if let trip = trip, let user = currentUser {
            userRef.childByAppendingPath(user.uid).childByAppendingPath("tripRequest").childByAppendingPath(trip.key).removeValue()
            userRef.childByAppendingPath(trip.userID).childByAppendingPath("trip").childByAppendingPath("friends").childByAppendingPath(user.uid).removeValue()
            tripsRef.childByAppendingPath(trip.key).childByAppendingPath("friends").childByAppendingPath(user.uid).removeValue()
        }
    }
    
}
