//
//  ContactViewController.swift
//  FollowMe
//
//  Created by Developer on 4/20/16.
//  Copyright © 2016 The Simple Studio. All rights reserved.
//

import UIKit

class ContactViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var friendTableView: UITableView!
    var contactInfo = [ContactInfo]()
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(loadContactInfo), name: "LOADCONTACT", object: nil)
        if let user = currentUser, let arr = user.contactArray as? [ContactInfo] {
            contactInfo = arr
        }
    }
    
    func loadContactInfo(notification: NSNotification) {
        if let userInfo = notification.userInfo,
            let contacts = userInfo["contact"] as? [ContactInfo]
        {
            contactInfo = contacts
            friendTableView.reloadData()
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Table View
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ContactCell") as? FriendCell
        cell?.cleanUI()
        cell?.nameLabel.text = contactInfo[indexPath.row].name
        cell?.subLabel.text = contactInfo[indexPath.row].phone
        return cell!
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactInfo.count
    }
}
