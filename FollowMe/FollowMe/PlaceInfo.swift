//
//  PlaceInfo.swift
//  FollowMe
//
//  Created by Developer on 5/1/16.
//  Copyright © 2016 The Simple Studio. All rights reserved.
//

import Foundation
import GoogleMaps

class PlaceInfo: NSObject, NSCoding {
    var name: String
    var address: String?
    var placeID: String?
    var coordinate: CLLocationCoordinate2D?
    
    func encodeWithCoder(aCoder: NSCoder) {
        //
        aCoder.encodeObject(self.address,forKey: "address")
        aCoder.encodeObject(self.name,forKey: "name")
        aCoder.encodeObject(self.placeID, forKey: "placeID")
        if let coordinate = self.coordinate {
            aCoder.encodeDouble(coordinate.latitude, forKey: "latitue")
            aCoder.encodeDouble(coordinate.longitude, forKey: "longitude")
        }
        
    }
    required convenience init?(coder decoder: NSCoder) {
        guard
            let name = decoder.decodeObjectForKey("name") as? String
            else { return nil }
        var coordinate: CLLocationCoordinate2D?
        if let lat = try? decoder.decodeDoubleForKey("latitue"),
            let long = try? decoder.decodeDoubleForKey("longitude") {
            coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
        }
        self.init(
            name: name,
            address: decoder.decodeObjectForKey("address") as? String,
            placeID: decoder.decodeObjectForKey("placeID") as? String,
            coordinate: coordinate
        )
    }
    
    required init(name: String) {
        self.name = name
    }
    
    init(name: String, address: String?, placeID: String?, coordinate: CLLocationCoordinate2D?) {
        self.name = name
        self.address = address
        self.placeID = placeID
        self.coordinate = coordinate
    }
    
    func toDictionary() -> NSDictionary {
        var addressTemp = ""
        var placeIDTemp = ""
        var lattitude = 0.0
        var lontitude = 0.0
        if let address = self.address {
            addressTemp = address
        }
        if let placeID = self.placeID {
            placeIDTemp = placeID
        }
        if let coor = self.coordinate {
            lattitude = coor.latitude
            lontitude = coor.longitude
        }
        return  [
            "name": name,
            "address": addressTemp,
            "id": placeIDTemp,
            "latitude": lattitude,
            "longitude": lontitude
        ]
        
    }
    
    class func fromDictionary(place: NSDictionary) -> PlaceInfo? {
        if let name = place["name"] as? String
        {
            let placeInfo = PlaceInfo(name: name)
            if let address = place["address"] as? String {
                placeInfo.address = address
            }
            if let placeID = place["id"] as? String {
                placeInfo.placeID = placeID
            }
            if let latitude = place["latitude"] as? Double, let longitude = place["longitude"] as? Double {
                placeInfo.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            }
            return placeInfo
            
        }
        return nil
    }
    
}