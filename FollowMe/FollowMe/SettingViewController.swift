//
//  SettingViewController.swift
//  FollowMe
//
//  Created by Developer on 4/19/16.
//  Copyright © 2016 The Simple Studio. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController {
    @IBOutlet weak var userName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        if let user = currentUser {
            userName.text = user.name
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func signOutAction(sender: AnyObject) {
        rootRef.unauth()
        Utility.openAuthentication(self)
    }
}
