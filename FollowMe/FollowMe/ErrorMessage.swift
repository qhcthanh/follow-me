//
//  ErrorMessage.swift
//  FollowMe
//
//  Created by Developer on 4/19/16.
//  Copyright © 2016 The Simple Studio. All rights reserved.
//

//Error notification
//Authentication
let kErrorNetwork =  "Network error, please check your connection."
let kErrorSignUpEmailExists = "This email's been registered, please try another."
let kErrorSignUpCantCreateUser = "Can't create account, please try again."
let kErrorEmailIsEmpty = "Email could not be empty!"
let kErrorPasswordIsEmpty = "Password could not be empty!"
let kErrorSignIsEmpty = "Email and Password could not be empty"
let kErrorSignInUserDoesNotExist = "User's not existed."
let kErrorSignInInvalidEmail = "Not a valid email, try again."
let kErrorSignInInvalidPassword = "Password is not correct!"
let kErrorAuthenticationDefault = "Something went wrong, please try again."
//Verification
let kErrorPhoneExists = "This phone's been registered please try another"
let kErrorUserNameShort = "This username's short. Username must be at least 5 character."
let kErrorPhoneShort = "This phone's short. Phone must be at least 7 characters."
let kErrorPhoneLong = "Phone number is too long, 15 characters is maximum."
let kErrorInputEmpty = "The username and phone couldn't be empty!"

//AddFriend
let kErrorAddFriendAddSelf = "You can not add yourself."
let kErrorCantFind = " We can't find. Do you want invite your friend to Follow Me?"
