//
//  User.swift
//  
//
//  Created by Developer on 4/19/16.
//
//

import Foundation
import CoreData
import Firebase
import GoogleMaps

class User: NSManagedObject {

// Insert code here to add functionality to your managed object subclass
    @NSManaged var uid: String
    @NSManaged var name: String
    @NSManaged var email: String
    @NSManaged var phone: String?
    @NSManaged var friends: NSData?
    @NSManaged var location: NSData?
    @NSManaged var toFollows: NSData?
    @NSManaged var fromFollows: NSData?
    @NSManaged var contact: NSData?
    
    lazy var contactArray: NSArray? = {
        if let data = self.contact {
            return NSKeyedUnarchiver.unarchiveObjectWithData(data) as? NSArray
        }
        return nil
    }()
    
    lazy var toFollowArray: NSArray? = {
        if let data = self.toFollows {
            return NSKeyedUnarchiver.unarchiveObjectWithData(data) as? NSArray
        }
        return nil
    }()
    
    lazy var fromFollowArray: NSArray? = {
        if let data = self.fromFollows {
            return NSKeyedUnarchiver.unarchiveObjectWithData(data) as? NSArray
        }
        return nil
    }()
    
    lazy var friendsDictionary: NSDictionary? = {
        if let data = self.friends {
            return NSKeyedUnarchiver.unarchiveObjectWithData(data) as? NSDictionary
        }
        return nil
    }()
    
    var currentLocation: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
    
    override init(entity: NSEntityDescription, insertIntoManagedObjectContext context: NSManagedObjectContext?) {
        super.init(entity: entity, insertIntoManagedObjectContext: context)
    }
    
    class func createInManagedObjectContext(moc: NSManagedObjectContext, uid: String, data: NSDictionary ) -> User {
        let newItem: User = NSEntityDescription.insertNewObjectForEntityForName("User", inManagedObjectContext: moc) as! User
        newItem.reloadData(uid, data: data)
        return newItem
    }
    
    func toDictionary() -> NSDictionary {
        let data = [
            "name": name,
            "email": email,
            "phone": phone!
        ]
        return data
    }
    
    func reloadData(uid: String, data: NSDictionary) {
        self.uid = uid
        if let em = data["email"] as? String {
            self.email =  em
        }
        if let name = data["name"] as? String {
            self.name = name
        }
        if let phone = data["phone"] as? String {
            self.phone = phone
        }
        if let mFriends = data["friends"] as? NSDictionary {
            print(mFriends)
            self.friends = NSKeyedArchiver.archivedDataWithRootObject(mFriends)
        }
        if let contact = data["contact"] as? NSArray {
            var contactsData = [ContactInfo]()
            for ele in contact {
                if let con = ele as? NSDictionary {
                    if let name = con["name"] as? String,
                        let phone = con["phone"] as? String {
                        if let imgStr = con["image"] as? String,
                            let imgData =  NSData(base64EncodedString: imgStr, options: .IgnoreUnknownCharacters) {
                            contactsData.append(ContactInfo(name: name,phone: phone,image: UIImage(data: imgData)))
                        } else {
                            contactsData.append(ContactInfo(name: name,phone: phone))
                        }
                    }
                    
                }
            }
            self.contact = NSKeyedArchiver.archivedDataWithRootObject(contactsData)
        }
        
        Utility.saveContext(nil,fail: nil)
    }
    
    class func all(moc: NSManagedObjectContext) -> [User]? {
        let fetchRequest = NSFetchRequest(entityName: "User")
        if let fetchResults = (try? moc.executeFetchRequest(fetchRequest)) as? [User] {
            return fetchResults
        }
        return []
    }
    
    class func getUserWithID(moc: NSManagedObjectContext, uID: String) -> User? {
        let fetchRequest = NSFetchRequest(entityName: "User")
        let predicate = NSPredicate(format: "uid = %@",uID)
        fetchRequest.predicate = predicate
        if let fetchResult = (try? moc.executeFetchRequest(fetchRequest) as? [User]) {
            if let firstItem = fetchResult?.first {
                return firstItem
            }
        }
        return nil
    }
    
    class func fromSnapshot(snapshot : FDataSnapshot) -> [FriendInfo] {
        var result = [FriendInfo]()
        for ele in snapshot.children {
            let eleData = ele as! FDataSnapshot
            let dict = eleData.value as? NSDictionary
            if let dictValue = dict {
                if let email = dictValue["email"] as? String,
                    let name = dictValue["name"] as? String,
                    let phone = dictValue["phone"] as? String {
                    let friend = FriendInfo(uid: eleData.key, name: name, email: email, phone: phone)
                    result.append(friend)
                    continue
                }
            }
        }
        return result
    }
    
//    class func bookmarks(moc: NSManagedObjectContext) -> [User]? {
//        let fetchRequest = NSFetchRequest(entityName: "User")
//        let firstPredicate = NSPredicate(format: "isBookmark == true")
//        fetchRequest.predicate = firstPredicate
//        if let fetchResults = (try? moc.executeFetchRequest(fetchRequest)) as? [User] {
//            return fetchResults
//        }
//        return []
//    }
    
    class func createNewUser(uid:String,email:String,name: String ) {
        // create user success
        let toAppointments = []
        let inviteAppointments = []
        let friends = [FriendInfo]()
        // set properties
        let newUser = [
            "email": email,
            "name": name,
            "toAppointments": toAppointments,
            "inviteAppointments": inviteAppointments,
            "friends": friends
        ]
        userRef.childByAppendingPath(uid).setValue(newUser)
        currentUser = User.createInManagedObjectContext(appDelegate.managedObjectContext, uid: uid, data: newUser)
        if let _ = try? appDelegate.managedObjectContext.save() {
            // Save ok
            print("Coredata save OK")
        }
        else {
            // Save error
            print("Coredata save Error")
        }
        
    }

}

//        if let mAppointment = data["toAppointments"] as? NSDictionary {
//            self.toAppointmentsData =  NSKeyedArchiver.archivedDataWithRootObject(mAppointment)
//        }
//        if let mInviteAppointment = data["fromAppointments"] as? NSDictionary {
//            self.fromAppointmentsData = NSKeyedArchiver.archivedDataWithRootObject(mInviteAppointment)
//        }
//        if let mFriends = data["friends"] as? NSDictionary {
//            print(mFriends)
//            self.myFriendsData = NSKeyedArchiver.archivedDataWithRootObject(mFriends)
//        }
//        if let mRequest = data["friendRequest"] as? NSDictionary {
//            self.friendRequestData = NSKeyedArchiver.archivedDataWithRootObject(mRequest)
//        }
//        if let phone = data["phone"] as? String {
//            self.phone = phone
//        }
//        if let address = data["address"] as? String {
//            self.address = address
//        }
//        if let birthday = data["birthday"] as? String {
//            let format = NSDateFormatter()
//            format.dateFormat = "dd, MMMM, yyyy"
//            self.birthday = format.dateFromString(birthday)
//        }
//        if let sex = data["sex"] as? String {
//            self.sex = sex
//        }
//        if let photo = data["photo"] as? String {
//            self.photo = NSData(base64EncodedString: photo, options: .IgnoreUnknownCharacters)
//        }
//        if let contact = data["contact"] as? NSArray {
//            var contactsData = [ContactInfo]()
//            for ele in contact {
//                if let con = ele as? NSDictionary {
//                    if let name = con["name"] as? String,
//                        let phone = con["phone"] as? String {
//                        if let imgStr = con["image"] as? String,
//                            let imgData =  NSData(base64EncodedString: imgStr, options: .IgnoreUnknownCharacters) {
//                            contactsData.append(ContactInfo(name: name,phone: phone,image: UIImage(data: imgData)))
//                        } else {
//                            contactsData.append(ContactInfo(name: name,phone: phone))
//                        }
//                    }
//
//                }
//            }
//            self.contact = NSKeyedArchiver.archivedDataWithRootObject(contactsData)
//        }

