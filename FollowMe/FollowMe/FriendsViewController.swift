//
//  FriendsViewController.swift
//  FollowMe
//
//  Created by Developer on 4/19/16.
//  Copyright © 2016 The Simple Studio. All rights reserved.
//

import UIKit

class FriendsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var friendTableView: UITableView!
    var friendInfo = [FriendInfo]()
    override func viewDidLoad() {
        super.viewDidLoad()
        if let user = currentUser {
            userRef.childByAppendingPath(user.uid).childByAppendingPath("friends").observeEventType(.Value, withBlock: {
                snapshot in
                let friends = User.fromSnapshot(snapshot)
                FriendInfo.loadToCurrentUser(friends)
                self.friendInfo = friends
                self.friendTableView.reloadData()
                if let _ = try? appDelegate.managedObjectContext.save() {
                    // Save ok
                    print("Coredata save OK")
                }
                else {
                    // Save error
                    print("Coredata save Error")
                }
            })
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Table View
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("FriendCell") as! FriendCell
        cell.cleanUI()
        cell.friend = friendInfo[indexPath.row]
        cell.renderUI()
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return friendInfo.count
    }
    
}
