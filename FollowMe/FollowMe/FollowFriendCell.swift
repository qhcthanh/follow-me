//
//  FollowFriendCell.swift
//  FollowMe
//
//  Created by Developer on 4/19/16.
//  Copyright © 2016 The Simple Studio. All rights reserved.
//

import UIKit

class FollowFriendCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var distance_time_Label: UILabel!
    @IBOutlet weak var activeImageView: UIImageView!
    
}
